import React from 'react';
const Card = ({
	imgWrapper,
	cardType,
	cardText,
	cardClick,
	button,
	textClass,
	buttonText,
	ratingClass,
	ratingValue,
	buttonClick,
}) => {
	return (
		<div className={cardType}>
			<div className={imgWrapper} onClick={cardClick}></div>
			{ratingValue && <span className={ratingClass}>{ratingValue}</span>}
			<span className={textClass ? textClass : 'text-base font-medium'}>
				{cardText}
			</span>
			{button ? (
				<button className={button} onClick={buttonClick}>
					{buttonText}
				</button>
			) : null}
		</div>
	);
};
export default Card;
