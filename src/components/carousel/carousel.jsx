import React, { useState } from 'react';
import { useSwipeable } from 'react-swipeable';

export const CarouselItem = ({ children, width }) => {
	return (
		<div
			className={`carousel-card inline-flex items-center justify-start text-center w-[48%] bg-transparent`}>
			{children}
		</div>
	);
};

const Carousel = ({ children }) => {
	const [activeIndex, setActiveIndex] = useState(0);
	const updateIndex = newIndex => {
		if (newIndex < 0) {
			newIndex = React.Children.count(children) - 2;
		} else if (newIndex >= React.Children.count(children) - 1) {
			newIndex = 0;
		}

		setActiveIndex(newIndex);
	};

	const handlers = useSwipeable({
		onSwipedLeft: () => updateIndex(activeIndex + 1),
		onSwipedRight: () => updateIndex(activeIndex - 1),
	});
	return (
		<div className="truncate" {...handlers}>
			<div
				// style, потому что через класс translate-x-[-$activeIndex * 50}%] работало почему-то через раз
				className="whitespace-nowrap transition"
				style={{ transform: `translate(-${activeIndex * 45}%)` }}>
				{React.Children.map(children, (child, index) => {
					return React.cloneElement(child);
				})}
			</div>
		</div>
	);
};
export default Carousel;
