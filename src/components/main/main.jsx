import React from 'react';
import PlantCards from './plant-cards.jsx';
import RecommendCarousel from './recommend-carousel.jsx';

const Main = () => {
	return (
		<main className="px-2">
			<div className="max-w-5xl mx-auto mt-[20px]">
				<h2 className="main-header text-lg">Recomended for you</h2>
				<RecommendCarousel />
			</div>
			<div
				className="banner bg-gradient-to-b from-banner-first to-banner-second cursor-pointer"
				onClick={() => console.log('banner-click')}>
				<div className="w-4/5">
					<h3 className="text-bg font-semibold text-white">
						Rubber-bearing ficus (elastic)
					</h3>
					<p className="text-sm font-normal text-white/50 line-clamp-2 mt-1 mb-0 ">
						Ficus elastica (Ficus elastica), or rubber-bearing ficus (Ficus
						elastica), or rubber-bearing ficus
					</p>
				</div>
				<div className="bg-ficus w-[50px] h-[84px] bg-center"></div>
			</div>
			<div>
				<div className="flex justify-between">
					<h2 className="main-header text-lg">Plants and flowers</h2>
					<button
						className="text-link-green bg-transparent focus:text-link-blue"
						onClick={() => console.log('see all plants')}>
						See all
					</button>
				</div>
				<div className="flex flex-wrap justify-around">
					<PlantCards />
				</div>
			</div>
		</main>
	);
};

export default Main;
