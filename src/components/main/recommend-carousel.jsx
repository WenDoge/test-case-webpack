import React from 'react';
import Card from '../card/card.jsx';
import Carousel, { CarouselItem } from '../carousel/carousel.jsx';

const RecommendCarousel = () => {
	return (
		<>
			<Carousel>
				<CarouselItem>
					<Card
						cardClick={() => console.log('obi-click')}
						cardType="card "
						imgWrapper={`carousel-img bg-obi`}
						cardText="ОБИ"
					/>
				</CarouselItem>
				<CarouselItem>
					<Card
						cardClick={() => console.log('leroy-click')}
						cardType="card"
						imgWrapper={`carousel-img bg-leroy`}
						cardText="Леруа Мерлен"
					/>
				</CarouselItem>
				<CarouselItem>
					<Card
						cardClick={() => console.log('clumba-click')}
						cardType="card"
						imgWrapper={`carousel-img bg-clumba`}
						cardText="Клумба"
					/>
				</CarouselItem>
				<CarouselItem>
					<Card
						cardClick={() => console.log('buket-click')}
						cardType="card"
						imgWrapper={`carousel-img bg-buket`}
						cardText="Букет Бар"
					/>
				</CarouselItem>
			</Carousel>
		</>
	);
};
export default RecommendCarousel;
