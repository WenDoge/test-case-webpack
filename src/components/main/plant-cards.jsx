import React from 'react';
import Card from '../card/card.jsx';

const PlantCards = () => {
	return (
		<>
			<Card
				cardClick={() => console.log('Orchid-click')}
				cardType="plants-card flex flex-col bg-white"
				imgWrapper="plants-img bg-[url('../assets/plants/orchid.png')]" //почему то неработает bg-orchid (добавлено в конфиг)
				cardText="Orchid"
				button="bg-button-green rounded-lg py-[10px] m-2"
				buttonText="Review"
				textClass={'text-base font-medium mb-4'}
				ratingValue="8.0"
				ratingClass="plants-rate bg-rating-orange text-xs -mt-2 pl-2.5"
				buttonClick={() => console.log('Orchid Review')}
			/>
			<Card
				cardClick={() => console.log('Pions-click')}
				cardType="plants-card flex flex-col bg-white"
				imgWrapper="plants-img bg-[url('../assets/plants/pions.png')]" //то же самое
				cardText="Pions"
				button="bg-button-green rounded-lg py-[10px] m-2"
				buttonText="Review"
				textClass={'text-base font-medium mb-4 pl-1'}
				ratingValue="9.0"
				ratingClass="plants-rate bg-rating-orange text-xs -mt-2 pl-2.5"
				buttonClick={() => console.log('Pions Review')}
			/>
			<Card
				cardClick={() => console.log('Aloe-click')}
				cardType="plants-card flex flex-col bg-white"
				imgWrapper="plants-img bg-[url('../assets/plants/aloe.png')]" //то же самое
				cardText="Aloe"
				button="bg-button-green rounded-lg py-[10px] m-2"
				buttonText="Review"
				textClass={'text-base font-medium mb-4'}
				ratingValue="5.6"
				ratingClass="plants-rate bg-rating-orange text-xs -mt-2 pl-2.5"
				buttonClick={() => console.log('Aloe Review')}
			/>
			<Card
				cardClick={() => console.log('Chrys-click')}
				cardType="plants-card flex flex-col bg-white"
				imgWrapper="plants-img bg-[url('../assets/plants/Chrysanthemum.png')]" //то же самое
				cardText="Chrysanthemum"
				button="bg-button-green rounded-lg py-[10px] m-2"
				buttonText="Review"
				textClass={'text-base font-medium mb-4'}
				ratingValue="7.5"
				ratingClass="plants-rate bg-rating-orange text-xs -mt-2 pl-2.5"
				buttonClick={() => console.log('Chrys Review')}
			/>
		</>
	);
};

export default PlantCards;
