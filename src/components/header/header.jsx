import React, { useState, useEffect, useRef } from 'react';
const Header = () => {
	const [navVisibility, setNavVisibility] = useState(false);
	const containerRef = useRef(null);

	const handleClickOutside = event => {
		if (containerRef.current && !containerRef.current.contains(event.target)) {
			setNavVisibility(false);
			return;
		}
	};
	useEffect(() => {
		document.addEventListener('mousedown', handleClickOutside);
		return () => {
			document.removeEventListener('mousedown', handleClickOutside);
		};
	});
	return (
		<header className="bg-main-black py-2 relative" ref={containerRef}>
			<div className="flex max-w-5xl justify-around items-center py-2 mx-auto">
				<div
					className="bg-logo w-36 h-[30px]"
					onClick={() => console.log('logo-click')}></div>
				<div className="bars">
					<button
						className="bg-transparent bg-glass bg-center w-8 h-8 mr-4"
						onClick={() => console.log('glass-click')}></button>
					<button
						className="bg-transparent bg-language bg-center w-8 h-8"
						onClick={() => console.log('lang-click')}></button>
					<button
						className="bg-transparent bg-bars bg-center w-8 h-8 ml-4"
						onClick={() => setNavVisibility(!navVisibility)}></button>
				</div>
			</div>
			{navVisibility && (
				<div className="bg-main-black nav transition">
					<button
						className="bg-button-gray signup-button py-2.5 rounded-lg text-white w-full"
						onClick={() => console.log('sign up click')}>
						<span className="signup-span">Sign up or Log in</span>
					</button>
					<ul className="list-none">
						<li
							className="py-5 border-b-2 border-white border-solid border-x-0 border-t-0 cursor-pointer"
							onClick={() => console.log('Plants click')}>
							Plants and flowers
						</li>
						<li
							className="py-5 border-b-2 border-white border-solid border-x-0 border-t-0 cursor-pointer"
							onClick={() => console.log('Care click')}>
							Plant Care
						</li>
						<li
							className="py-5 cursor-pointer"
							onClick={() => console.log('News click')}>
							News
						</li>
					</ul>
				</div>
			)}
		</header>
	);
};
export default Header;
