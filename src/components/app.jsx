import React from 'react';
import '../styles/main.css';
import Header from './header/header.jsx';
import Main from './main/main.jsx';

const App = () => {
	return (
		<div className="bg-main-gray">
			<Header />
			<Main />
		</div>
	);
};
export default App;
