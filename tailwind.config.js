module.exports = {
	corePlugins: {
		preflight: false,
	},
	content: ['./src/**/*.{html,js,jsx}'],
	theme: {
		extend: {
			backgroundImage: {
				logo: "url('/src/assets/Logo.svg')",
				glass: "url('/src/assets/magnifying-glass.svg')",
				language: "url('/src/assets/Language.svg')",
				bars: "url('/src/assets/bars.svg')",
				obi: "url('/src/assets/carousel-comp/obi.jpg')",
				leroy: "url('/src/assets/carousel-comp/leroy.jpg')",
				clumba: "url('/src/assets/carousel-comp/clumba.jpg')",
				buket: "url('/src/assets/carousel-comp/buket.jpg')",
				ficus: "url('/src/assets/ficus.png')",
				orchid: "url('/src/assets/plants/orchid.png)",
				pions: "url('/src/assets/plants/pions.png)",
				aloe: "url('/src/assets/plants/aloe.png)",
				chrys: "url('/src/assets/plants/chrysanthemum.png)",
			},
			colors: {
				'main-black': '#111827',
				'main-gray': '#E5E5E5',
				'banner-first': '#CBD623',
				'banner-second': '#41701E',
				'link-green': '#6BA91A',
				'link-blue': '#2563EB',
				'button-green': '#E2FEDB',
				'rating-orange': '#F59E0B',
				'button-gray': '#1F2937',
			},
			fontFamily: {
				sans: ['Graphik', 'sans-serif'],
				serif: ['Merriweather', 'serif'],
			},
			fontSize: {
				sm: ['14px', '20px'],
				base: ['16px', '20px'],
				lg: ['18px', '24px'],
				xs: ['12px', '14px'],
			},
			boxShadow: {
				main: '0px 2px 4px rgba(0, 0, 0, 0.08)',
			},
		},
	},
	plugins: [require('@tailwindcss/line-clamp')],
};
